<?php

namespace Database\Seeders;

use App\Models\Config;
use Illuminate\Database\Seeder;

class ConfigSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('configs')->delete();

        Config::create([
            'meta_title' => 'Bem Vindo ao Meu Site',
            'meta_description' => '',
            'meta_keywords' => '',
            'name_site' => 'Meu Site',
            'proprietary' => 'Leonardo',
            'address' => '',
            'email_admin' => 'contato@innsystem.com.br',
            'telephone' => '',
            'cellphone' => '',
            'hour_open' => '',
            'logo' => '/sem_imagem.jpg',
        ]);
    }
}
