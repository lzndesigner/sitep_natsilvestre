@extends('auth.app')

@section('content')
<div class="box p-10 rounded30 box-shadowed">
    <div class="box-body">
        {{ __('Please confirm your password before continuing.') }}

        <form method="POST" action="{{ route('password.confirm') }}">
            @csrf

            <div class="form-group row">
                <label for="password" class="col-md-12 col-form-label text-md-left">{{ __('Senha de Acesso') }}</label>

                <div class="col-md-12">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                        name="password" required autocomplete="current-password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row mt-4 mb-0">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Confirmar Senha') }}
                    </button>

                    @if (Route::has('password.request'))
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            {{ __('Esqueceu sua Senha?') }}
                        </a>
                    @endif
                </div>
            </div>
        </form>
        <p class="m-2"><a href="{{ url('/dashboard') }}">Voltar ao Dashboard</a></p>
    </div>
</div>
@endsection
