@extends('auth.app')

@section('content')
<div class="box p-10 rounded30 box-shadowed">
    <div class="box-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('password.email') }}">
            @csrf

            <div class="form-group row">
                <label for="email" class="col-md-12 col-form-label text-md-left">{{ __('E-Mail de Acesso') }}</label>

                <div class="col-md-12">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                        value="{{ old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row mt-4 mb-0">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Enviar link para Redefinir Senha') }}
                    </button>
                </div>
            </div>
        </form>
        <p class="m-2"><a href="{{ url('/login') }}">Voltar ao Login</a></p>
    </div>
</div>
@endsection
