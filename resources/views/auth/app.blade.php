<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Vendors Style-->
    <link rel="stylesheet" href="{{ asset('backend/css/vendors_css.css?' . rand()) }}">

    <!-- Style-->
    <link rel="stylesheet" href="{{ asset('backend/css/style.css?' . rand()) }}">
    <link rel="stylesheet" href="{{ asset('backend/css/skin_color.css?' . rand()) }}">
    <link rel="stylesheet" href="{{ asset('backend/css/login_custom.css?' . rand()) }}">

    <!-- Font-Awesome -->
    <script src="https://kit.fontawesome.com/04571ab3d2.js" crossorigin="anonymous"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

</head>

<body class="hold-transition theme-primary bg-gradient-primary">
    <div id="app" class="h-p100">
        <div class="container h-p100">
            <div class="row align-items-center justify-content-md-center h-p100">

                <div class="col-12">
                    <div class="row justify-content-center no-gutters">
                        <div class="col-lg-6 col-md-8 col-12">

                            <div class="content-top-agile p-10">
                                <img src="/galerias/logo_innsystem_white.png" alt="InnSystem Inovação em Sistemas"
                                    class="img-fluid col-9 mb-4">
                                <h2 class="text-white">Painel de Controle</h2>
                            </div>

                            @yield('content')

                            <p class="mt-4 text-center">Todos os direitos reservados <br>
                                <b>InnSystem Inovação em Sistemas</b> 2010~{{ date('Y') }}©
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- #app -->
    <!-- Vendor JS -->
    <script src="{{ asset('backend/js/vendors.min.js?' . rand()) }}"></script>
</body>

</html>
