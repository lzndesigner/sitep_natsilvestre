<!DOCTYPE html>
<html lang="pt_BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$config_site->meta_title}}</title>

    <base href="https://lp.natsilvestre.com.br/" />
    <!-- // add url image product to head -->
    <meta name="robots" content="follow" />
    <meta name="googlebot" content="index, follow, all" />
    <meta name="language" content="pt-br" />
    <meta name="revisit-after" content="3 days">
    <meta name="rating" content="general" />
    <meta property="og:url" content="https://natsilvestre.com.br/" />
    <meta property="og:title" content="{{$config_site->meta_title}}" />
    <meta name="description" content="{{$config_site->meta_description}}" />
    <meta property="og:description" content="{{$config_site->meta_description}}" />
    <!-- // end: add url image product to head -->
    <meta name="keywords" content="{{$config_site->meta_keywords}}" />
    <meta property="og:locale" content="pt_BR" />
    <meta property="og:type" content="website" />
    <meta property="og:image:url" content="https://natsilvestre.com.br/facebook.png" />
    <meta property="og:image:type" content="image/png" />

    <link rel="shortcut icon" href="{{ asset('/favicon3.ico?1') }}" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600&family=Open+Sans:wght@300;400;500;600&display=swap" rel="stylesheet">
    <link href="{{ asset('css/app.css?6') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('frontend/css/template.css?6'.rand()) }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('plugins/animate.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="{{ asset('/plugins/swiper/css/swiper.min.css') }}" />
    @yield('pageCSS')
    <link rel="stylesheet" href="{{ asset('/plugins/lei_lgpd.css?1') }}" />
    
    <link rel="stylesheet" href="{{ asset('/plugins/fontawesome/css/all.min.css?1') }}" />
</head>

<body class="">

    <main>
        <section class="section section-form">
            <div class="container">
                <div class="row justify-content-center">

                    <div class="col-12 col-md-12 col-lg-8 mt-5 text-sm-center">
                        <h1>Aromaterapia para <br> Gestantes e Puérperas</h1>
                        <p>Aprenda sobre Aromaterapia em Gestantes e puérperas e comece a atuar em um dos mercados que mais crescem no mundo. Torne-se uma Consultora em aromaterapia e tenha mais segurança e um amplo conhecimento no uso dos óleos essenciais.</p>
                    </div><!-- cols -->

                </div><!-- row -->

                <div class="row mt-5 section-motives align-items-center">
                    <div class="col-12 col-md-12 col-lg-7">
                        <h3>Motivos para aprender:</h3>
                        <ul class="list-unstyled">
                            <li><i class="fa fa-square-check text-success"></i> Viver e envelhecer com mais saúde.</li>
                            <li><i class="fa fa-square-check text-success"></i> Acolher a gestante com segurança e autonomia</li>
                            <li><i class="fa fa-square-check text-success"></i> Aumentar a sensação de bem-estar físico e emocional.</li>
                            <li><i class="fa fa-square-check text-success"></i> Ter um sistema imunológico mais resistente a ataques virais e bacterianos.</li>
                            <li><i class="fa fa-square-check text-success"></i> Ter mais equilíbrio emocional.</li>
                            <li><i class="fa fa-square-check text-success"></i> Reduzir a carga tóxica no organismo.</li>
                            <li><i class="fa fa-square-check text-success"></i> Ajudar você e sua família a ter uma vida mais saudável.</li>
                            <li><i class="fa fa-square-check text-success"></i> Trabalhar com Aromaterapia e Óleos Essenciais.</li>
                        </ul>
                    </div>
                    <div class="col-12 col-md-12 col-lg-5 mb-md-5">
                        <div class="image text-center text-lg-left">
                            <img src="{{ asset('/galerias/aromaterapia-para-a-casa.webp') }}" alt="Aromaterapia em Gestantes" class="img-fluid img-rounded img-rounded-5">
                        </div>

                    </div><!-- cols -->
                </div>
            </div><!-- container -->
        </section>

        <section class="section section-about">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-6 mb-md-5">
                        <div class="image text-center text-lg-left">
                            <img src="{{ asset('/galerias/natalia/7-1.jpg') }}" alt="Natalia Silvestre" class="col-8 col-md-6 col-lg-10 img-fluid img-rounded-100">
                        </div>

                    </div><!-- cols -->

                    <div class="col-12 col-md-12 col-lg-6 mt-5 text-sm-center">
                        <h2>Natália Silvestre</h2>

                        <p>Eu sou Natália Silvestre, tenho 36 anos, casada com o Gustavo e mãe do Enzo de 6 anos! Me dedico ao estudo da Aromaterapia desde a adolescência, mas o amor pela terapia cresceu ainda mais após minha gestação e o nascimento do meu Filho. Ver o poder da Aromaterapia nos desconfortos da gestação e num puerpério para lá de difícil me fez decidir querer mais!</p>
                        <p>Me formei Doula e consultora em Aleitamento Materno em 2018, e então, passei a me dedicar profissionalmente ao estudo e formação da Aromaterapia, unindo as práticas de forma integrada. Perceber como tudo se conecta me fascinou e vi a importância de um olhar diferenciado a esse momento da vida. Graduada e filiada na AromaFlora, além de devoradora de Robert Tisserand e outros autores da Ed. Laszlo, também sou Administradora com pós graduação em Varejo e criadora do Sinergia do Nascer.</p>

                    </div><!-- cols -->

                </div><!-- row -->
            </div><!-- container -->
        </section>
        <section class="section section-reviews">
            <div class="container p-5">
                <div class="row">
                    <div class="col-12 col-sm-12 col-lg-4 mb-5 text-center">
                        <h4 class="mb-4"><i class="fab fa-instagram"></i> <br> Siga no Instagram <br /> <a href="https://www.instagram.com/natzsilvestre/" target="_Blank">@natzsilvestre</a></h2>
                            <a href="https://www.instagram.com/natzsilvestre/" target="_Blank" class="effect-zoom"><img src="{{ asset('galerias/others/mockup_celular.png') }}" alt="Siga-me no Instagram" class="img-fluid img-rounded-5"></a>
                    </div><!-- cols -->
                    <div class="col-12 col-sm-12 col-lg-8">
                        <h2>O que as mulheres estão falando...</h2>

                        <!-- Swiper -->
                        <div class="swiper mySwiper pb-5">
                            <div class="swiper-wrapper">
                                @include('frontend.includes.reviews')
                            </div>
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div><!-- cols -->
                </div><!-- row -->
            </div><!-- container -->
        </section>

        <section class="section section-curso mb-2">
            <div class="container">
                <h2>Curso de Aromaterapia</h2>
                <p>Voltado para profissionais que atuam com gestantes e puérperas para que você possa atuar com segurança e responsabilidade. Nele, você vai aprender desde a história dos óleos, formas de extração até conhecer profundamente os principais óleos e entender as melhores formas de aplicação em todo o ciclo gravídico puerperal.</p>
                @include('frontend.includes.box-info')

                <div class="mt-5 text-center">
                    <a href="https://lp.natsilvestre.com.br" target="_Blank" class="btn btn-lg btn-success">Quero me inscrever agora</a>
                </div>
            </div><!-- container -->
        </section>

        <section class="section section-redesociais text-center">
            <div class="container">
                <h2>Siga nas Redes Sociais</h2>

                <ul class="mb-4">
                    <li><a href="https://www.instagram.com/natzsilvestre/" target="_Blank"><i class="fab fa-instagram"></i></a></li>
                    <li><a href="https://www.facebook.com/natzsilvestre" target="_Blank"><i class="fab fa-facebook"></i></a></li>
                </ul>

                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fnatzsilvestre&tabs&width=340&height=130&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1094654680570525" width="340" height="130" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
            </div>
        </section>
    </main>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 text-center">
                    <p>Natália Silvestre {{ date('Y') }} ©. Todos os direitos reservados</p>
                </div>
            </div>
        </div>
        <div id="toTop" class="btn" style="display: block;"><span class="fa fa-chevron-up" aria-hidden="true"></span></div>
    </footer>


    <div id="box-privacity-ctn" class="box-privacity-ctn" style="display:none">
        <div class="box-privacity d-flex-column d-sm-flex-column d-md-flex align-items-center" id="box-privacity">
            <div class="flex-grow-1 mb-3 mb-sm-3 mb-md-0 mr-md-3">
                <p><b>Privacidade e os cookies</b>: a gente usa cookies para personalizar anúncios e melhorar a sua experiência no
                    site. <br> Ao continuar navegando, você concorda com a nossa <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#exampleModal">Termos de Uso</a>.</p>
            </div>
            <div class="box-privacity-newsletter">
                <a href="javascript:buttonClose();" id="button-close" class="btn btn-primary btn-sm">Continuar e Fechar</a>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Termos de Uso & Serviços</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    @include('frontend.termos_uso')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('frontend/js/jquery-3.6.0.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="{{ asset('frontend/js/template.js?2') }}"></script>
    @yield('pageJS')
    <!-- Swiper JS -->
    <script src="{{ asset('/plugins/swiper/js/swiper.min.js') }}"></script>

    <!-- Initialize Swiper -->
    <script>
        var swiper = new Swiper(".mySwiper", {
            centeredSlides: true,
            autoplay: {
                delay: 2500,
                disableOnInteraction: false,
            },
            loop: true,
            slidesPerView: 1,
            spaceBetween: 10,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            breakpoints: {
                640: {
                    slidesPerView: 1,
                    spaceBetween: 20,
                },
                768: {
                    slidesPerView: 1,
                    spaceBetween: 30,
                },
                1024: {
                    slidesPerView: 2,
                    spaceBetween: 30,
                },
            },
        });
    </script>

    <script src="{{ asset('/plugins/jquery.cookie.js') }}"></script>
    <script type='text/javascript'>
        $(function() {

            if ($.cookie('box-pop-up') == null) {

                $("#box-privacity").click(function(e) {
                    e.stopPropagation();
                });

                $("#button-close").click(function(e) {
                    $('#box-privacity-ctn').fadeOut();
                });

                $('#box-privacity-ctn').show();
            }

            var date = new Date();

            // date.setTime(date.getTime() + (365 * 24 * 60 * 60)); //year
            date.setTime(date.getTime() + (30 * 60 * 1000)); //30 min
            // date.setTime(date.getTime() + (10)); //10 segundos

            $.cookie('box-pop-up', 'box-pop', {
                expires: date
            });
        });
    </script>
</body>

</html>