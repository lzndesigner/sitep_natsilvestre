<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Sinergia do Nascer</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content=" " />
    <meta name="keywords" content="" />

    <link rel="shortcut icon" href="images/favicon.ico">

    <!-- Bootstrap css -->
    <link rel="stylesheet" href="{{ asset('/frontend/css/bootstrap.min.css') }}" type="text/css" id="bootstrap-style" />

    <!-- Material Icon Css -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@mdi/font@6.5.95/css/materialdesignicons.min.css" type="text/css" />

    <!-- Unicon Css -->
    <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.0/css/line.css" />

    <!-- Swiper Css -->
    <link rel="stylesheet" href="{{ asset('/frontend/css/tiny-slider.css?'.rand()) }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/frontend/css/swiper.min.css?'.rand()) }}" type="text/css" />

    <!-- Custom Css -->
    <link rel="stylesheet" href="{{ asset('/frontend/css/style.css?'.rand()) }}" type="text/css" />

    <!-- colors -->
    <link href="{{ asset('/frontend/css/colors/default.css?'.rand()) }}" rel="stylesheet" type="text/css" id="color-opt" />
    <link href="{{ asset('/frontend/css/template.css?'.rand()) }}" rel="stylesheet" type="text/css" id="color-opt" />

</head>

<body data-bs-spy="scroll" data-bs-target="#navbar" data-bs-offset="71">

    <!-- START  NAVBAR -->
    <nav class="navbar navbar-expand-lg fixed-top navbar-custom sticky sticky-light" id="navbar">
        <div class="container-fluid">

            <!-- LOGO -->
            <a class="navbar-brand logo text-uppercase" href="index-1.html">
                <img src="images/logo-light.png" class="logo-light" alt="" height="30">
                <img src="images/logo-dark.png" class="logo-dark" alt="" height="30">
            </a>

            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="mdi mdi-menu"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ms-auto" id="navbar-navlist">
                    <li class="nav-item">
                        <a class="nav-link" href="#inicio">Início</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#sobre_curso">Conheça sobre</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#utilizacao">Quem pode utilizar</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#depoimentos">Depoimentos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#descricao">Descrição do Curso</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#duvidas">Dúvidas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#sobre_nat">Sobre a Nat</a>
                    </li>
                </ul>
                <div class="ms-auto">
                    <a href="https://pay.hotmart.com/Q46455928U?checkoutMode=0&bid=1624895460288&split=12&offDiscount=PRIMEIRODIA&_hi=eyJjaWQiOiIxNjM1NDUwNzE1NDEyNTEyNzEyNzAzNjgxMDA5MzAwIiwiYmlkIjoiMTYzNTQ1MDcxNTQxMjUxMjcxMjcwMzY4MTAwOTMwMCIsInNpZCI6IjExYTYyOTNmMDg3OTQ3NThiYTljYWVlNTQ3OWZjZjUyIn0=.1647438890979" target="_Blank" class="btn bg-gradiant">Inscrição</a>
                </div>
            </div>
        </div>
    </nav>
    <!-- END NAVBAR -->



    <!-- home section -->
    <section class="home bg-light" id="inicio">
        <!-- start container -->
        <div class="container">
            <!-- start row -->
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <h1 class="display-5 fw-bold">AROMATERAPIA APLICADA DA GESTAÇÃO AO PUERPÉRIO</h1>
                    <p class="mt-4 f-22 l-h-32 text-dark">Aprenda a utilizar os óleos essenciais com toda segurança e responsabilidade mesmo se você nunca tiver ouvido falar sobre aromaterapia</p>
                    <a href="https://pay.hotmart.com/Q46455928U?checkoutMode=0&bid=1624895460288&split=12&offDiscount=PRIMEIRODIA&_hi=eyJjaWQiOiIxNjM1NDUwNzE1NDEyNTEyNzEyNzAzNjgxMDA5MzAwIiwiYmlkIjoiMTYzNTQ1MDcxNTQxMjUxMjcxMjcwMzY4MTAwOTMwMCIsInNpZCI6IjExYTYyOTNmMDg3OTQ3NThiYTljYWVlNTQ3OWZjZjUyIn0=.1647438890979" target="_Blank" class="btn btn-lg bg-gradiant mt-4">Inscreva-se já!</a>
                </div>



                <div class="col-lg-5 offset-md-1 ">
                    <img src="{{ asset('/galerias/natalia/7.jpg') }}" alt="Natalia Silvestre" class="img-fluid border-radius">
                    <div class="up-botton-text d-flex align-items-center d-none d-md-block">
                        <h5>Nat Silvestre 🙋‍♀️</h5>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->

        <div class="background-line"></div>
    </section>
    <!-- end home section -->

    <!-- service section -->
    <section class="section service bg-light" id="sobre_curso">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="title text-center mb-5">
                        <h6 class="mb-0 fw-bold text-dark">Conheça mais sobre</h6>
                        <h2 class="f-40">AROMATERAPIA!</h2>
                    </div>
                </div>
            </div>

            <div class="row justify-content-between">
                <div class="col-lg-4">
                    <div class="service-box text-center shadow">
                        <div class="service-icon p-1">
                            <i class="mdi mdi-flower text-gradiant f-30"></i>
                        </div>

                        <div class="service-content mt-4">
                            <h5 class="fw-bold">INTRODUÇÃO À AROMATERAPIA</h5>
                            <p class="text-muted">Conhecer a História da Aromaterapia, o que são e como são feitos o Óleos Essenciais e os meios de funcionamento como Absorção inalatória, Cutânea e Ingestão.</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 pt-4 pt-lg-0">
                    <div class="service-box text-center shadow">
                        <div class="service-icon p-1">
                            <i class="mdi mdi-flower-outline text-gradiant f-30"></i>
                        </div>

                        <div class="service-content mt-4">
                            <h5 class="fw-bold">APLICAÇÃO E REGRAS DE USO</h5>
                            <p class="text-muted">Vamos conhecer a segurança de uso dos Óleos Essenciais na Gestação e todas as suas formas de aplicação durante todo o ciclo Gravídico-Puerperal, incluindo armazenamento e as contra-indicações</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 pt-4 pt-lg-0">
                    <div class="service-box text-center shadow">
                        <div class="service-icon p-1">
                            <i class="mdi mdi-flower-pollen text-gradiant f-30"></i>
                        </div>

                        <div class="service-content mt-4">
                            <h5 class="fw-bold">COLOCANDO EM PRÁTICA</h5>
                            <p class="text-muted">As diferenças entre Óleos Essenciais, Hidrolato, Vegetais e Essências, onde comprar, marcas, valores e a elaboração se Sinergias e Diluições na prática.</p>
                        </div>
                    </div>
                </div>
            </div><!-- row -->


            <div class="row justify-content-between my-3">
                <div class="col-lg-4">
                    <div class="service-box text-center shadow">
                        <div class="service-icon p-1">
                            <i class="mdi mdi-flower-pollen-outline text-gradiant f-30"></i>
                        </div>

                        <div class="service-content mt-4">
                            <h5 class="fw-bold">ÓLEOS ESSENCIAIS</h5>
                            <p class="text-muted">Estudos individuais de cada Óleo Essencial. Os permitidos e os não liberados, quando e como usar cada um deles nas diferentes fases da Gestação e Parto.</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 pt-4 pt-lg-0">
                    <div class="service-box text-center shadow">
                        <div class="service-icon p-1">
                            <i class="mdi mdi-flower-poppy text-gradiant f-30"></i>
                        </div>

                        <div class="service-content mt-4">
                            <h5 class="fw-bold">ÓLEOS VEG E CARREGADORES</h5>
                            <p class="text-muted">A escolha dos Óleos Vegetais e Carregadores, seus usos e a influência deles na ação dos Óleos Essenciais. Como lidar com o uso de Óleos Cosméticos</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 pt-4 pt-lg-0">
                    <div class="service-box text-center shadow">
                        <div class="service-icon p-1">
                            <i class="mdi mdi-flower-tulip text-gradiant f-30"></i>
                        </div>

                        <div class="service-content mt-4">
                            <h5 class="fw-bold">INDICANDO OS ÓLEOS</h5>
                            <p class="text-muted">Como fazer uma anamnese responsável de suas clientes para pensar em indicações e restrições baseadas em histórico médico, preferências e memórias olfativas, responsabilidade de uso e reações adversas.</p>
                        </div>
                    </div>
                </div>
            </div><!-- row -->


            <div class="row justify-content-center">
                <div class="col-lg-4">
                    <div class="service-box text-center shadow">
                        <div class="service-icon p-1">
                            <i class="mdi mdi-gift-open-outline text-gradiant f-30"></i>
                        </div>

                        <div class="service-content mt-4">
                            <h5 class="fw-bold">BÔNUS E MUITO MAIS</h5>
                            <p class="text-muted">Receitas Coringas para Ansiedade, Azia e Enjôo e o Parto, Puerpério, O Uso da Aromaterapia na vida do Profissional e muito mais...</p>
                        </div>
                    </div>
                </div>
            </div><!-- row -->

        </div>
    </section>
    <!-- end section -->

    <!-- start features -->
    <div class="section features" id="utilizacao">
        <!-- start container -->
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="title text-center mb-5">
                        <h2 class="f-40">Quem pode se <br> Aprofundar e Utilizar?</h2>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center">


                <div class="col-lg-12">
                    <div class="tab-content mt-2" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">

                            <div class="row align-items-center">
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="features-box mt-4">
                                                <div class="d-flex">
                                                    <div class="icon">
                                                        <i class="mdi mdi-check-circle f-20 me-2"></i>
                                                    </div>
                                                    <div class="heading">
                                                        <p class="text-muted">Todos os profissionais que atuam com o ciclo gravídico-puerperal e que desejam segurança e responsabilidade no uso da Aromaterapia</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="features-box mt-4">
                                                <div class="d-flex">
                                                    <div class="icon">
                                                        <i class="mdi mdi-check-circle f-20 me-2"></i>
                                                    </div>
                                                    <div class="heading">
                                                        <p class="text-muted">Obstetras, Obstetrizes, Enfermeiras Obstétricas e Doulas</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="features-box mt-4">
                                                <div class="d-flex">
                                                    <div class="icon">
                                                        <i class="mdi mdi-check-circle f-20 me-2"></i>
                                                    </div>
                                                    <div class="heading">
                                                        <p class="text-muted">Consultoras de Aleitamento Materno, Doulas pós parto e Terapeutas</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="features-box mt-4">
                                                <div class="d-flex">
                                                    <div class="icon">
                                                        <i class="mdi mdi-check-circle f-20 me-2"></i>
                                                    </div>
                                                    <div class="heading">
                                                        <p class="text-muted">Fisioterapeutas, Psicólogas, Nutricionistas e Educadoras Perinatais</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="features-box mt-4">
                                                <div class="d-flex">
                                                    <div class="icon">
                                                        <i class="mdi mdi-check-circle f-20 me-2"></i>
                                                    </div>
                                                    <div class="heading">
                                                        <p class="text-muted">Quem já estuda sobre aromaterapia mas deseja se aprofundar no assunto</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="features-box mt-4">
                                                <div class="d-flex">
                                                    <div class="icon">
                                                        <i class="mdi mdi-check-circle f-20 me-2"></i>
                                                    </div>
                                                    <div class="heading">
                                                        <p class="text-muted">Quem atua no parto e deseja levar mais formas de apoio as mulheres</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="features-box mt-4">
                                                <div class="d-flex">
                                                    <div class="icon">
                                                        <i class="mdi mdi-check-circle f-20 me-2"></i>
                                                    </div>
                                                    <div class="heading">
                                                        <p class="text-muted">Quem gostaria de levar mais conforto e leveza aos partos que atende com auxilio no alívio da dor, estímulos naturais e uma experiência positiva.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="features-box mt-4">
                                                <div class="d-flex">
                                                    <div class="icon">
                                                        <i class="mdi mdi-check-circle f-20 me-2"></i>
                                                    </div>
                                                    <div class="heading">
                                                        <p class="text-muted">Quem deseja utilizar Aromaterapia em seu dia a dia</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="features-box mt-4">
                                                <div class="d-flex">
                                                    <div class="icon">
                                                        <i class="mdi mdi-check-circle f-20 me-2"></i>
                                                    </div>
                                                    <div class="heading">
                                                        <p class="text-muted">Qualquer iniciante profissional da área e que nunca tenha estudado sobre Aromaterapia</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="features-box mt-4">
                                                <div class="d-flex">
                                                    <div class="icon">
                                                        <i class="mdi mdi-check-circle f-20 me-2"></i>
                                                    </div>
                                                    <div class="heading">
                                                        <p class="text-muted">Quem deseja agregar valor ao seu trabalho e levar mais qualidade de vida para suas clientes</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="features-box mt-4">
                                                <div class="d-flex">
                                                    <div class="icon">
                                                        <i class="mdi mdi-check-circle f-20 me-2"></i>
                                                    </div>
                                                    <div class="heading">
                                                        <p class="text-muted">Quem deseja complementar seus conhecimentos, seus atendimentos e possibilidades</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="features-box mt-4">
                                                <div class="d-flex">
                                                    <div class="icon">
                                                        <i class="mdi mdi-check-circle f-20 me-2"></i>
                                                    </div>
                                                    <div class="heading">
                                                        <p class="text-muted">Quem precisa de um gás para lidar com as emoções após um dia de atendimentos</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- end container -->
    </div>
    <!-- end features -->


    <!-- start testimonial -->
    <section class="section testimonial" id="depoimentos">
        <!-- start container -->
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 text-center">
                    <div class="title">
                        <p class="text-dark fw-bold mb-0">Depoimentos de Alunas <i class="mdi mdi-comment"></i>
                        </p>
                        <h3>O que as mulheres <br> estão dizendo?</h3>
                    </div>
                </div>
            </div>
            <div class="row align-items-center mt-4">
                <div class="col-lg-12">
                    <div class="testi-slider" id="testi-slider">

                        <div class="item">
                            <div class="testi-box position-relative overflow-hidden">
                                <div class="row align-items-center">
                                    <div class="col-md-12">
                                        <div class="p-4">
                                            <div class="mt-3">
                                                <h5 class="fw-bold">MARI DIAS <small class="fw-normal text-muted mb-0 f-14"> - DOULA E CONSULTORA EM ALEITAMENTO MATERNO</small></h5>
                                                <p class="text-muted f-14">Eu admiro muito a Nat! Acompanho sua página porque é alguém muito comprometida com a Aromaterapia e fornece conteúdos excelentes. Adquiri uma Sinergia para usar em parto com as minhas clientes e estou muito satisfeita!</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="back-image position-absolute end-0 bottom-0">
                                    <img src="{{ asset('/frontend/images/testi/rating-image.png') }}" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <!-- slider item -->

                        <div class="item">
                            <div class="testi-box position-relative overflow-hidden">
                                <div class="row align-items-center">
                                    <div class="col-md-12">
                                        <div class="p-4">
                                            <div class="mt-3">
                                                <h5 class="fw-bold">MARI CORAIOLA <small class="fw-normal text-muted mb-0 f-14"> - DOULA, EDUCADORA PERINATAL E MENTORA DE DOULAS</small></h5>
                                                <p class="text-muted f-14">Conheci a Nat há muitos anos e fui sua Doula no nascimento do Enzo. Hoje, quando preciso de apoio e novos conhecimentos em Aromaterapia, sou eu que recorro a ela! Profissional competente, estudiosa e muito responsável, sempre muito assertiva em seus ensinamentos de Aromaterapia na Gestação, Parto e na Vida!</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="back-image position-absolute end-0 bottom-0">
                                    <img src="{{ asset('/frontend/images/testi/rating-image.png') }}" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <!-- slider item -->

                        <div class="item">
                            <div class="testi-box position-relative overflow-hidden">
                                <div class="row align-items-center">
                                    <div class="col-md-12">
                                        <div class="p-4">
                                            <div class="mt-3">
                                                <h5 class="fw-bold">CRISTINA MAYA <small class="fw-normal text-muted mb-0 f-14"> - DOULANDA</small></h5>
                                                <p class="text-muted f-14">“Conheci a aromaterapia através da Nat durante a minha gestação e meus incômodos e ansiedade diminuíram muito! E inclusive, foi essencial durante meu trabalho de parto. Desde então, sou amante dos óleos essenciais, minha casa vive com um difusor ligado e tanto eu, quanto meu marido, com um colar difusor cada, que minha bebê ama e já sente os efeitos junto. Alias, até no processo de adaptação dela (e minha!) no berçário foi importantíssimo. Hoje aprendemos também a diluir em óleos vegetais, a indicar para a família e a presentear nossos amigos que ao conhecer a aromaterapia, estão se encantando com todos os benefícios. Gratidão, Nat!”</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="back-image position-absolute end-0 bottom-0">
                                    <img src="{{ asset('/frontend/images/testi/rating-image.png') }}" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <!-- slider item -->

                        <div class="item">
                            <div class="testi-box position-relative overflow-hidden">
                                <div class="row align-items-center">
                                    <div class="col-md-12">
                                        <div class="p-4">
                                            <div class="mt-3">
                                                <h5 class="fw-bold">POLIANA ANDRO <small class="fw-normal text-muted mb-0 f-14"> - CONSULTORIA INDIVIDUAL</small></h5>
                                                <p class="text-muted f-14">Queria te relatar hoje que o Arthur ontem a noite veio me agradecer por eu estar usando o Óleo Essencial que a Nat me aconselhou com ele. Foi a noite, depois do banho, quando geralmente ele fica muito constipada. Ele agradeceu, ai respirou fundo, e disse: "olha mãe, ate sinto o seu cheiro!". Não tenho palavras pra te agradecer por isso, Nat. Estamos usando só aquele blend, a exatamente uma semana! Obrigada!</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="back-image position-absolute end-0 bottom-0">
                                    <img src="{{ asset('/frontend/images/testi/rating-image.png') }}" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <!-- slider item -->


                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-12 text-center">
                    <a href="https://www.youtube.com/channel/UCwBTl1hRiDOdXSpooFwN5mg" target="_Blank" class="btn bg-gradiant mt-4">Veja no Youtube <i class="mdi mdi-youtube"></i></a>
                </div>
            </div>
        </div>
        <!-- end container -->
    </section>
    <!-- end testimonial -->

    <!-- cta section -->
    <section class="section cta" id="cta">
        <div class="bg-overlay-gradiant"></div>
        <!-- start container -->
        <div class="container position-relative">
            <div class="row justify-content-center">
                <div class="col-lg-10 text-center">
                    <div class="py-5">
                        <h1 class="display-4 text-white">Curso registrado na Abrath e na Biblioteca Nacional RDA</h1>
                        <p class="text-white-50 mt-3 f-18">O Sinergia do Nascer tem registro na Biblioteca Nacional RDA e na Abrath (Associação Brasileira de Terapeutas Holísticos), sendo aceito para contabilização de horas para para compor novas filiações, bem como comprovar capacitações técnicas.</p>
                        <img src="{{ asset('/galerias/certificado_curso.jpeg') }}" alt="Certificado Curso" class=" img-fluid">
                    </div>

                </div>
            </div>
        </div>
        <!-- end container -->
    </section>
    <!-- end section -->

    <!-- pricing section -->
    <section class="section pricing" id="descricao">
        <div class="container">


            <div class="row justify-content-center align-items-center">
                <div class="col-lg-7">
                    <div class="row justify-content-center">
                        <div class="col-lg-12">
                            <div class="title text-left mb-5">
                                <h2 class="f-40">Descrição do Curso</h2>
                                <p class="text-muted">Curso de Aromaterapia voltado para profissionais que atuam com gestantes e puérperas para que você possa atuar com segurança e responsabilidade.</p>
                                <p class="text-muted">Nele, você vai aprender desde a história dos óleos, formas de extração até conhecer profundamente os principais óleos e entender as melhores formas de aplicação em todo o ciclo gravídico puerperal</p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="px-2">
                                <li>01) Módulo 01 - Introdução à aromaterapia</li>
                                <li>02) Módulo 02 - Aplicação e regras gerais de uso</li>
                                <li>03) Módulo 03 - Colocando em prática</li>
                                <li>04) Módulo 04 - Indicando os óleos</li>
                                <li>05) Módulo 05 - Óleos vegetais e veículos carregadores</li>
                                <li>06) Módulo 06 - Óleos essenciais</li>
                                <li>07) Módulo 07 - O ciclo gravídico-puerperal</li>
                                <li>08) Módulo 08 - Sintomas e sensações</li>
                                <li>09) Módulo 09 - Autocuidado profissional</li>
                                <li>10) E-books</li>
                                <li>11) Aulas bônus</li>
                                <li>12) Aulas ao vivo</li>
                                <li>13) Encerramento</li>
                                <li>14) Bônus - Jornada Aromaparto - jan/2021</li>
                                <li>15) Bônus - Jornada Aromaparto - abr/2021</li>
                                <li>16) Bônus - Jornada Aromaparto - jul/2021</li>
                                <li>17) Bônus - Jornada Aromaparto - fev/2022</li>
                            </ul>
                        </div>
                    </div>

                </div>
                <div class="col-lg-4 offset-1 ">
                    <div class="row justify-content-center">
                        <div class="col-lg-12">
                            <div class="title text-center mb-5">
                                <h2 class="f-40">Investimento</h2>
                                <p class="text-muted">Escolha a melhor forma de pagamento para você!</p>

                            </div>
                        </div>
                    </div>
                    <div class="price-item shadow-sm overflow-hidden mt-4 mt-lg-0">
                        <div class="topbar-header bg-primary py-2 text-center">
                            <h6 class="mb-0 text-white fw-normal">Bônus para as 20 primeiras</h6>
                        </div>
                        <div class="price-up-box p-4">
                            <div class="price-tag mt-2 text-center">
                                <h2 class="text-dark mb-0 f-40 l-h-32">
                                    <sup class="f-16 fw-normal">em até 12x de </sup><br>
                                    <sup class="f-22 fw-normal">R$</sup>97,14
                                </h2>
                            </div>
                            <div class="price-tag mt-4 text-center">
                                <sup class="f-16 fw-normal">ou <b>R$ 997,00</b> à vista</sup>
                            </div>
                        </div>

                        <div class="price-down-box  text-center p-4">
                            <ul class="list-unstyled">
                                <li><i class="mdi mdi-check-circle f-20 align-middle me-2 text-primary"></i> Acesso durante 12 meses</li>
                                <li class="mt-2"><i class="mdi mdi-check-circle f-20 align-middle me-2 text-primary"></i> Grupo exclusivo no WhatsApp</li>
                                <li class="mt-2"><i class="mdi mdi-check-circle f-20 align-middle me-2 text-primary"></i> Acesso 100% Online</li>
                            </ul>
                            <a href="https://pay.hotmart.com/Q46455928U?checkoutMode=0&bid=1624895460288&split=12&offDiscount=PRIMEIRODIA&_ga=2.79718483.536458792.1642101875-1862470597.1642101875&_hi=eyJjaWQiOiIxNjM1NDUwNzE1NDEyNTEyNzEyNzAzNjgxMDA5MzAwIiwiYmlkIjoiMTYzNTQ1MDcxNTQxMjUxMjcxMjcwMzY4MTAwOTMwMCIsInNpZCI6IjExYTYyOTNmMDg3OTQ3NThiYTljYWVlNTQ3OWZjZjUyIn0=.1647451918546" target="_Blank" class="btn btn-lg btn-primary mt-3" style="font-size:16px;"><i class="mdi mdi-check-all me-2"></i> Quero começar agora!</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row justify-content-between mt-5">
                <div class="col-lg-4">
                    <div class="service-box text-center">
                        <div class="service-icon p-2 color-purple">
                            <i class="mdi mdi-lock-open-check-outline f-50"></i>
                        </div>

                        <div class="service-content mt-2">
                            <h5 class="fw-bold color-purple">ACESSO 12 MESES</h5>
                            <p class="text-dark f-14">Acesso ilimitado ao conteúdo durante 12 meses. Você assistir quantas vezes quiser!</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 pt-4 pt-lg-0">
                    <div class="service-box text-center">
                        <div class="service-icon p-2 color-green">
                            <i class="mdi mdi-whatsapp f-50"></i>
                        </div>

                        <div class="service-content mt-2">
                            <h5 class="fw-bold color-green">GRUPO WHATSAPP</h5>
                            <p class="text-dark f-14">Grupo de apoio no WhatsApp com a sua turma para dúvidas, trocas de experiências por 12 meses e depois um grupo no Telegram com todas as turmas.</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 pt-4 pt-lg-0">
                    <div class="service-box text-center">
                        <div class="service-icon p-2 color-blue">
                            <i class="mdi mdi-wifi f-50"></i>
                        </div>

                        <div class="service-content mt-2">
                            <h5 class="fw-bold color-blue">ACESSO 100% ONLINE</h5>
                            <p class="text-dark f-14">Conteúdo totalmente Online, inclusive com opção de assistir OffLine no App Sparkle Hotmart e assistir onde quiser.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end pricing -->

    <!-- contact section -->
    <section class="section contact overflow-hidden" id="duvidas">
        <!-- start container -->
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="title text-center mb-5">
                        <h2 class="f-40">Principais Dúvidas!</h2>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center">
                <div class="col-lg-9">
                    <div class="faq-questions">
                        <div class="faq-question faq-accordion">
                            <h3 class="title">1 – Já tenho curso de Aromaterapia, vale a pena fazer mais esse curso? Já utilizo técnicas básicas com as gestantes!</h3>
                            <div class="content">
                                <div class="text">Sim, o curso de Aromaterapia para Profissionais do Parto, Sinergia do Nascer, possui como seu foco principal o aprofundamento das técnicas específicas voltadas à Gestação, Parto e Pós-Parto, para que você possa aplicá-las com segurança e responsabilidade em seus atendimentos. Sair do básico pode ser a grande diferença na sua atuação!</div>
                            </div>
                        </div>
                        <div class="faq-question faq-accordion">
                            <h3 class="title">2 – Preciso ter alguma formação específica para aplicar as técnicas que serão ensinadas no curso? Tem certificado?</h3>
                            <div class="content">
                                <div class="text">O conteúdo do curso é voltado para todos os profissionais que atuam em qualquer fase da Gestação, Parto e Pós-Parto. Ele é um complemento a sua formação principal. Sim, ao fim do curso é emitido um certificado de conclusão com carga horária de 50H/aula com certificado reconhecido pela biblioteca RDA e ABRATH</div>
                            </div>
                        </div>
                        <div class="faq-question faq-accordion">
                            <h3 class="title">3 – Como funcionará o grupo de WhatsApp para Alunos, será aberto para tirar dúvidas?</h3>
                            <div class="content">
                                <div class="text">Sim! O Grupo de WhatsApp para alunos é aberto e com a minha presença, para tirar todas as suas dúvidas, além disso, o grupo terá outros profissionais para muita troca de informações, experiências e uma excelente rede de apoio. </div>
                            </div>
                        </div>
                        <div class="faq-question faq-accordion">
                            <h3 class="title">4 – Qual a duração do grupo de WhatsApp?</h3>
                            <div class="content">
                                <div class="text">O grupo de WhatsApp tem duração definida de 12 meses. Após esse prazo as alunas serão adicionadas a um grupo de Telegram com todas as&nbsp; alunas de todas as turmas do Sinergia do Nascer.</div>
                            </div>
                        </div>
                        <div class="faq-question faq-accordion">
                            <h3 class="title">5 – Qual será o tempo que terei de acesso ao curso?</h3>
                            <div class="content">
                                <div class="text">O curso ficará disponível de forma irrestrita por 12 meses! As aulas são gravadas em vídeo e Você poderá assisti-las quantas vezes quiser, inclusive a plataforma que escolhemos é a Hotmart, onde você pode usar o App Sparkle em seu celular ou tablet e baixar as aulas para assistir off-line de onde você tiver, sem precisar gastar seu plano de dados quando estiver sem acesso a rede wi-fi.</div>
                            </div>
                        </div>
                        <div class="faq-question faq-accordion">
                            <h3 class="title">6 – E se eu não gostar do curso?</h3>
                            <div class="content">
                                <div class="text">O curso possui garantia incondicional de 15 dias, ou seja, se em 26 dias você não gostar do conteúdo, achar que não te agregou ou qualquer outro motivo particular, é só pedir o estorno diretamente na plataforma que sua compra será estornada, sem demais questionamentos.</div>
                            </div>
                        </div>
                        <div class="faq-question faq-accordion">
                            <h3 class="title">7 – Posso me inscrever até que dia?</h3>
                            <div class="content">
                                <div class="text">As inscrições vão até dia 03/02 ás 23:59h e ainda NÃO existe previsão para abertura da próxima turma para esse curso, pois eu preciso de um tempo para me dedicar a esse grupo que está sendo iniciado.</div>
                            </div>
                        </div>
                        <div class="faq-question faq-accordion">
                            <h3 class="title">8 – Posso me juntar com outras pessoas para comprar o curso e compartilhar a senha?</h3>
                            <div class="content">
                                <div class="text"> Não pode! Cada matrícula da direito a um participante. A plataforma que utilizamos é a Hotmart e eles possuem um controle de acesso pela identificação de IP e localidade, podendo bloquear o acesso e notificar os produtores do curso. Isso é Ilegal!</div>
                            </div>
                        </div>
                        <div class="faq-question faq-accordion">
                            <h3 class="title Medium Medium-rich">9 – Posso parcelar?</h3>
                            <div class="content">
                                <div class="text">Sim, você pode parcelar em até 12 vezes.</div>
                            </div>
                        </div>
                        <div class="faq-question faq-accordion">
                            <h3 class="title">10 – Posso parcelar no boleto?</h3>
                            <div class="content">
                                <div class="text">Não será possível o parcelamento em boleto, mas você pode escolher a opção de parcelamento recorrente na propria hotmart para não comprometer o cartão</div>
                            </div>
                        </div>
                        <div class="faq-question faq-accordion">
                            <h3 class="title">11 - E como eu recupero o meu investimento que fiz no curso? </h3>
                            <div class="content">
                                <div class="text">Além de ter mais uma excelente ferramenta e agregar mais valor aos atendimentos que você já faz hoje, ele irá te proporcionar um grande diferencial. Você poderá ampliar o seu portfólio fazendo consultorias para as mulheres, preparar e vender as suas próprias sinergias, ou seja, você cobra pela consultoria, identifica as necessidades, cria a sinergia e vende para as suas clientes. Tenho certeza que você vai encontrar o melhor formato para explorar todo o conhecimento que será agregado a você! </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row justify-content-center mt-4">
                <div class="col-md-3">
                    <div class="d-flex align-items-center mt-4 mt-lg-0">
                        <div class="flex-shrink-0">
                            <i class="mdi mdi-email f-50 text-primary"></i>
                        </div>
                        <div class="flex-grow-1 ms-3">
                            <h5 class="mb-1">Email</h5>
                            <p class="f-14 mb-0 text-muted"><a href="mailto:contato@natsilvestre.com.br" target="_Blank">contato@natsilvestre.com.br</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="d-flex align-items-center mt-4 mt-lg-0">
                        <div class="flex-shrink-0">
                            <i class="mdi mdi-whatsapp f-50 text-primary"></i>
                        </div>
                        <div class="flex-grow-1 ms-3">
                            <h5 class="mb-1">WhatsApp</h5>
                            <p class="f-14 mb-0 text-muted"><a href="https://api.whatsapp.com/send?phone=5511970655806" target="_Blank">(11) 9.7065-5806</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end container -->
    </section>
    <!-- end section -->


    <!-- footer section -->
    <section class=" section footer bg-dark overflow-hidden" id="sobre_nat">
        <div class="bg-arrow">

        </div>
        <!-- container -->
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="title text-center text-white mb-5">
                        <h2 class="f-40">Conheça a <br> Natália Silvestre!</h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-md-12 col-lg-6 mb-md-5">
                    <div class="image text-center text-lg-left">
                        <img src="https://natsilvestre.com.br/galerias/natalia/7-1.jpg" alt="Natalia Silvestre" class="col-8 col-md-6 col-lg-10 img-fluid img-rounded-100">
                    </div>

                </div><!-- cols -->

                <div class="col-12 col-md-12 col-lg-6 mt-5 text-sm-center text-white">

                    <p>Eu sou Natália Silvestre, tenho 36 anos, casada com o Gustavo e mãe do Enzo de 5 anos! Me dedico ao estudo da Aromaterapia desde a adolescência, mas o amor pela terapia cresceu ainda mais após minha gestação e o nascimento do meu Filho. Ver o poder da Aromaterapia nos desconfortos da gestação e num puerpério para lá de difícil me fez decidir querer mais!</p>
                    <p>Me formei Doula e consultora em Aleitamento Materno em 2018, e então, passei a me dedicar profissionalmente ao estudo e formação da Aromaterapia, unindo as práticas de forma integrada. Perceber como tudo se conecta me fascinou e vi a importância de um olhar diferenciado a esse momento da vida. Graduada e filiada na AromaFlora, além de devoradora de Robert Tisserand e outros autores da Ed. Laszlo, também sou Administradora com pós graduação em Varejo e criadora do Sinergia do Nascer.</p>

                </div><!-- cols -->

            </div>

        </div>
        <!-- end container -->
    </section>

    <section class="bottom-footer py-4">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <p class="mb-0 text-center text-muted">“Este produto não substitui o parecer profissional. <br> Sempre consulte um profissional da saúde para tratar de assuntos relativos a saúde.” <br>
                        As informações disponíveis nesse curso não substituem em nenhuma hipótese o parecer médico profissional. Sempre consulte o seu médico sobre qualquer assunto relativo à sua saúde e os tratamentos tomados por você ou pelas pessoas que recebem seus cuidados e atenção! <br> <br>
                        Os cursos livres têm como Base Legal o Decreto Presidencial N° 5.154, de 23 de julho de 2004. <br>
                        O Curso livre à distância é uma modalidade de educação não-formal de duração variável, destinada a proporcionar aos estudantes e trabalhadores conhecimentos que lhe permitam profissionalizar-se, qualificar-se e atualizar-se para o trabalho. <br>
                        Curso Livre – Lei nº 9.394/96 – Diretrizes e Bases da Educação Nacional passou a integrar a modalidade de Educação Profissional.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- end footer -->


    <!--Bootstrap Js-->
    <script src="{{ asset('/frontend/js/bootstrap.bundle.min.js?'.rand()) }}"></script>

    <!-- Slider Js -->
    <script src="{{ asset('/frontend/js/tiny-slider.js?'.rand()) }}"></script>
    <script src="{{ asset('/frontend/js/swiper.min.js?'.rand()) }}"></script>

    <!-- <script src="{{ asset('/frontend/js/smooth-scroll.polyfills.min.js') }}"></script> -->

    <!-- counter -->
    <!-- <script src="{{ asset('/frontend/js/counter.init.js') }}"></script> -->

    <!-- App Js -->
    <script src="{{ asset('/frontend/js/template.js?'.rand()) }}"></script>

    <script>
        var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
        var tooltipList = tooltipTriggerList.map(function(tooltipTriggerEl) {
            return new bootstrap.Tooltip(tooltipTriggerEl)
        })
    </script>

</body>

</html>