@extends('frontend.base')

@section('container')
<header class="section section-form">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-12 col-lg-6 mb-md-5">
                <div class="image text-center text-lg-left">
                    <img src="{{ asset('/galerias/natalia/7-1.jpg') }}" alt="Natalia Silvestre" class="col-8 col-md-6 col-lg-10 img-fluid img-rounded-100">
                </div>

            </div><!-- cols -->

            <div class="col-12 col-md-12 col-lg-6 mt-5 text-sm-center">
                <h1>Natália Silvestre</h1>

                <p>Eu sou Natália Silvestre, tenho 36 anos, casada com o Gustavo e mãe do Enzo de 5 anos! Me dedico ao estudo da Aromaterapia desde a adolescência, mas o amor pela terapia cresceu ainda mais após minha gestação e o nascimento do meu Filho. Ver o poder da Aromaterapia nos desconfortos da gestação e num puerpério para lá de difícil me fez decidir querer mais!</p>
                <p>Me formei Doula e consultora em Aleitamento Materno em 2018, e então, passei a me dedicar profissionalmente ao estudo e formação da Aromaterapia, unindo as práticas de forma integrada. Perceber como tudo se conecta me fascinou e vi a importância de um olhar diferenciado a esse momento da vida. Graduada e filiada na AromaFlora, além de devoradora de Robert Tisserand e outros autores da Ed. Laszlo, também sou Administradora com pós graduação em Varejo e criadora do Sinergia do Nascer.</p>

            </div><!-- cols -->

        </div><!-- row -->
    </div><!-- container -->
</header>
<main>
    <section class="section section-about">
        <div class="container p-5">
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-4 mb-5 text-center">
                    <h4 class="mb-4"><i class="fa fa-instagram"></i> <br> Siga no Instagram <br/> <a href="https://www.instagram.com/natzsilvestre/" target="_Blank">@natzsilvestre</a></h2>
                    <a href="https://www.instagram.com/natzsilvestre/" target="_Blank" class="effect-zoom"><img src="{{ asset('galerias/others/mockup_celular.png') }}" alt="Siga-me no Instagram" class="img-fluid img-rounded-5"></a>
                </div><!-- cols -->
                <div class="col-12 col-sm-12 col-lg-8">
                        <h2>O que as mulheres estão falando...</h2>

                    <!-- Swiper -->
                    <div class="swiper mySwiper pb-5">
                        <div class="swiper-wrapper">
                            @include('frontend.includes.reviews')
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div><!-- cols -->
            </div><!-- row -->
        </div><!-- container -->
    </section>

    <section class="section section-curso mb-2">
        <div class="container">
            <h2>Curso de Aromaterapia</h2>
            <p>Voltado para profissionais que atuam com gestantes e puérperas para que você possa atuar com segurança e responsabilidade. Nele, você vai aprender desde a história dos óleos, formas de extração até conhecer profundamente os principais óleos e entender as melhores formas de aplicação em todo o ciclo gravídico puerperal.</p>
            @include('frontend.includes.box-info')

            <div class="mt-5 text-center">
            <a href="https://lp.natsilvestre.com.br" target="_Blank" class="btn btn-lg btn-success">Quero me inscrever agora</a>
            </div>
        </div><!-- container -->
    </section>

    <section class="section section-redesociais text-center">
        <div class="container">
            <h2>Siga nas Redes Sociais</h2>

            <ul class="mb-4">
                <li><a href="https://www.instagram.com/natzsilvestre/" target="_Blank"><i class="fa fa-instagram"></i></a></li>
                <li><a href="https://www.facebook.com/natzsilvestre" target="_Blank"><i class="fa fa-facebook"></i></a></li>
            </ul>

            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fnatzsilvestre&tabs&width=340&height=130&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1094654680570525" width="340" height="130" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
        </div>
    </section>
</main>

@endsection

@section('pageCSS')
<!-- Include SweetAlert -->
<link rel="stylesheet" href="{{ asset('/plugins/sweetalert/sweetalert2.min.css') }}">

<!-- Link Swiper's CSS -->
<link rel="stylesheet" href="{{ asset('/plugins/swiper/css/swiper.min.css') }}" />
@endsection

@section('pageJS')
<!-- Include SweetAlert -->
<script src="{{ asset('/plugins/sweetalert/sweetalert2.min.js') }}"></script>
<script>
    $(document).on('click', '#submit', function(e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            }
        });

        var method = 'POST';
        var url = "{{ route('newcontact') }}";
        var data = $('#form-request').serialize();

        $.ajax({
            url: url,
            data: data,
            method: method,
            success: function(data) {
                // location.href = "{{ route('obrigado') }}";:
                // Swal.fire({
                //     text: data,
                //     icon: 'success',
                //     showClass: {
                //         popup: 'animate_animated animate_backInUp'
                //     },
                //     onClose: () => {
                //         // Redirect Page Listagem
                //         // console.log('teste');
                //     }
                // });
            },
            error: function(xhr) {
                if (xhr.status === 422) {
                    Swal.fire({
                        text: 'Validação: ' + xhr.responseJSON,
                        icon: 'warning',
                        showClass: {
                            popup: 'animate_animated animate_wobble'
                        }
                    });
                } else {
                    Swal.fire({
                        text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
                        icon: 'error',
                        showClass: {
                            popup: 'animate_animated animate_wobble'
                        }
                    });
                }
            }
        });
    });
</script>

<!-- Swiper JS -->
<script src="{{ asset('/plugins/swiper/js/swiper.min.js') }}"></script>

<!-- Initialize Swiper -->
<script>
    var swiper = new Swiper(".mySwiper", {
        centeredSlides: true,
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        },
        loop: true,
        slidesPerView: 1,
        spaceBetween: 10,
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        breakpoints: {
            640: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 1,
                spaceBetween: 30,
            },
            1024: {
                slidesPerView: 2,
                spaceBetween: 30,
            },
        },
    });
</script>
@endsection