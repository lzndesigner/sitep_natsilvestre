<div class="section-info">
    <div class="d-flex flex-column flex-lg-row">
        <div class="flex-fill mb-5 mb-lg-0">
            <i class="fa fa-calendar"></i>
            <p>Data de Realização <br />
                <b>{{$config_site->data_evento}}</b>
            </p>
        </div><!-- col -->
        <div class="flex-fill mb-5 mb-lg-0">
            <i class="fa fa-desktop"></i>
            <p>Evento 100% Online <br />
                Totalmente <b>GRATUITO</b></p>
        </div><!-- col -->
        <div class="flex-fill mb-5 mb-lg-0">
            <i class="fab fa-whatsapp"></i>
            <p>Conteúdo <b>EXCLUSIVO</b> <br />
                no grupo do WhatsApp</p>
        </div><!-- col -->
    </div>
</div>