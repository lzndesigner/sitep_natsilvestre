<div class="swiper-slide">
    <div class="review">
        <div class="review-details d-flex align-items-center">
            <div class="me-3">
                <img src="galerias/reviews/elis-meier.jpg" alt="Elis Meier" class="img-responsive">
            </div><!-- cols -->
            <div class="">
                <h4>Elis Meier</h4>
            </div>
        </div>
        <p>Eu amo suas dicas de uso seguro de óleos. Receitas simples que até mesmo quem não é da área pode usar com segurança.</p>
    </div><!-- review -->
</div><!-- swiper-slider -->


<div class="swiper-slide">
    <div class="review">
        <div class="review-details d-flex align-items-center">
            <div class="me-3">
                <img src="galerias/reviews/juliana-vieira.jpg" alt="Juliana Vieira" class="img-responsive">
            </div><!-- cols -->
            <div class="">
                <h4>Juliana Vieira</h4>
            </div>
        </div>
        <p>Eu comprei o curso para aprender o uso da aromaterapia em gestantes e trabalho de parto, me dar mais segurança e aprender desde o início, como fazer as diluições e tal... Adoro essa parte de aprender sobre a diluição e ter um raciocínio clínico do porquê fazer o uso de óleo X e suas diluições e poder falar desses óleos novos, diferentes e suas diversas indicações, usos, diluições, como usar.</p>
    </div><!-- review -->
</div><!-- swiper-slider -->


<div class="swiper-slide">
    <div class="review">
        <div class="review-details d-flex align-items-center">
            <div class="me-3">
                <img src="galerias/reviews/margot.jpg" alt="Margot" class="img-responsive">
            </div><!-- cols -->
            <div class="">
                <h4>Margot</h4>
            </div>
        </div>
        <p>Nat eu não estava buscando o curso, mas vi suas LIVES de lançamento e vc me passou segurança e ofertou o que eu precisava estudar e ainda não tinha achado em curso algum.</p>
        <p>Se hoje vc abrisse uma formação maior e eu pudesse pagar, faria mole, mole. Vc passa segurança nas informações que dá e adoraria poder estudar mais a fundo sobre isso com vc.</p>
    </div><!-- review -->
</div><!-- swiper-slider -->


<div class="swiper-slide">
    <div class="review">
        <div class="review-details d-flex align-items-center">
            <div class="me-3">
                <img src="galerias/reviews/gisela.jpg" alt="Gisela" class="img-responsive">
            </div><!-- cols -->
            <div class="">
                <h4>Gisela</h4>
            </div>
        </div>
        <p>Eu ainda não sou doula, mas pretendo fazer o curso ano que vem. Inicialmente quero atuar no ciclo da gestação e pós-parto, somente em 2023 pretendo começar a atender parto, que minha nenê já vai ter 1 ano. Eu já estou fazendo uma formação de fito aromaterapia e curso de florais, mas eu queria um curso específico que abordasse o uso dos óleos em gestantes, segurança, modo de usar, indicações, receitinhas para uso prático etc., e o teu curso me trouxe tudo que precisava. Foi para isso que busquei, para ter uma visão mais específica para esse público, pq achei que só uma formação "geral" não seria suficiente.</p>
    </div><!-- review -->
</div><!-- swiper-slider -->


<div class="swiper-slide">
    <div class="review">
        <div class="review-details d-flex align-items-center">
            <div class="me-3">
                <img src="galerias/reviews/ale.jpg" alt="Ale" class="img-responsive">
            </div><!-- cols -->
            <div class="">
                <h4>Ale</h4>
            </div>
        </div>
        <p>Eu comprei o curso porque a aromaterapia chamou muito minha atenção por ser uma terapia natural! Pensar que a aromaterapia pode auxiliar tanto as mulheres em um momento tão especial e ao mesmo tempo cheio de surpresas me encantou ainda mais! Amando o curso! </p>
    </div><!-- review -->
</div><!-- swiper-slider -->


<div class="swiper-slide">
    <div class="review">
        <div class="review-details d-flex align-items-center">
            <div class="me-3">
                <img src="galerias/reviews/liana.jpg" alt="Liana" class="img-responsive">
            </div><!-- cols -->
            <div class="">
                <h4>Liana</h4>
            </div>
        </div>
        <p>Nat, comprei o curso com total interesse na gestante e pós-parto. Todos os assuntos nesse público me atraem. Além, da conexão com vc!
            Estou quase terminando o curso… logo, estarei aqui comentando …, porém todos os dias participo (indiretamente) deste curso. Meninas, vocês também são demais!!!adoro esse grupo e vc Nat. </p>
    </div><!-- review -->
</div><!-- swiper-slider -->


<div class="swiper-slide">
    <div class="review">
        <div class="review-details d-flex align-items-center">
            <div class="me-3">
                <img src="galerias/reviews/dalmara.jpg" alt="Dalmara" class="img-responsive">
            </div><!-- cols -->
            <div class="">
                <h4>Dalmara</h4>
            </div>
        </div>
        <p>Bom dia. Comprei o curso porque sempre tive muito interesse em entender mais o universo dos aromas e suas relações com a gestação, parto e puerpério. Confesso q ainda não fiz o curso em sua totalidade, mas aos poucos vou aprendendo. Seria interessante falar um pouco mais sobre o uso dos hidrolatos no parto. E já que estamos falando sobre o universo feminino pq não estender aos problemas relacionados ao pós-parto? O uso para as dores pélvicas tb seria interessante. Enfim é somente sugestões para um curso já tão rico.</p>
    </div><!-- review -->
</div><!-- swiper-slider -->


<div class="swiper-slide">
    <div class="review">
        <div class="review-details d-flex align-items-center">
            <div class="me-3">
                <img src="galerias/reviews/valquiria.jpg" alt="Valquiria" class="img-responsive">
            </div><!-- cols -->
            <div class="">
                <h4>Valquiria</h4>
            </div>
        </div>
        <p>Eu comprei o curso porque queria aprender a usar a aromaterapia na gestação, parto e pós-parto, mas principalmente no pós-parto. Eu já fiz formação, cursos de aromaterapia na gestação e outros, mas nenhum me proporcionou o que encontrei aqui. Além de vc ser uma profissional top, superinteligente, sabe repassar, interage aqui e no privado, você ensina de uma forma fantástica.
            E ainda tem grupo no Whatsapp que é pura luz.</p>
    </div><!-- review -->
</div><!-- swiper-slider -->


<div class="swiper-slide">
    <div class="review">
        <div class="review-details d-flex align-items-center">
            <div class="me-3">
                <img src="galerias/reviews/grenda.jpg" alt="Grenda" class="img-responsive">
            </div><!-- cols -->
            <div class="">
                <h4>Grenda</h4>
            </div>
        </div>
        <p>Sou formada em aromaterapia, porém com Gestantes precisamos de segurança ao indicar e o Sinergia e do Nascer me trouxe essa segurança.</p>
    </div><!-- review -->
</div><!-- swiper-slider -->


<div class="swiper-slide">
    <div class="review">
        <div class="review-details d-flex align-items-center">
            <div class="me-3">
                <img src="galerias/reviews/juliana.jpg" alt="Juliana" class="img-responsive">
            </div><!-- cols -->
            <div class="">
                <h4>Juliana</h4>
            </div>
        </div>
        <p>Nat, eu comprei o curso porque não sabia usar nada e queria usar nas mulheres…, porém, Enquanto eu assistia as aulas, ia me interessando cada vez mais sobre o assunto. Eu queria muito que você fizesse um curso mais aprofundado. Que falasse sobre utilizar os olhos essenciais em crianças, o uso em pessoas fora da gestação. Gosto da sua didática. Queria fazer um curso, cheguei até a te perguntar na caixinha, mas morro de medo de fazer um curso com uma pessoa que não tem a sua maneira de explicar. </p>
    </div><!-- review -->
</div><!-- swiper-slider -->