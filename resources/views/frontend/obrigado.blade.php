@extends('frontend.base')

@section('container')
<header class="section section-form">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-12 col-lg-7 mt-5 mb-5 mb-md-5 mb-lg-0 text-sm-center">
                <h1>Obrigada por <br /> se cadastrar</h1>
                <h3><b>Mas espere</b>, sua inscrição <br> ainda não está totalmente concluída.</h3>

                <div class="row my-4">
                    <div class="col-12 col-md-6 offset-0 offset-sm-0 offset-lg-3">
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: 79%" aria-valuenow="79" aria-valuemin="0" aria-valuemax="100">79%</div>
                        </div>
                    </div>
                </div>

                <p>Todas as informações serão compartilhadas <br> no <b>Grupo Exclusivo no WhatsApp</b>.</p>

                <a href="{{ url('https://chat.whatsapp.com/IZUum9pu4HJ3ELP0jxO5ln') }}" target="_Blank" class="btn btn-lg btn-success animate__animated animate__heartBeat">Clique aqui e Participe do Grupo <i class="fa fa-whatsapp"></i></a>

            </div><!-- cols -->
            <div class="col-12 col-md-12 col-lg-5 mb-md-5">
                <div class="image text-center text-lg-left">
                    <img src="{{ asset('/galerias/natalia/6-1.jpg') }}" alt="" class="col-7 col-md-5 col-lg-10 img-fluid img-rounded-100">
                    <div class="logo">
                        <img src="{{ asset('/galerias/logo.jpg') }}" alt="Natalia Silvestre">
                    </div>
                </div>
            </div><!-- cols -->
        </div><!-- row -->
    </div><!-- container -->
</header>
<main>
    <section class="section mb-2">
        <div class="container">
            @include('frontend.includes.box-info')
        </div><!-- container -->
    </section>

</main>
@endsection

@section('pageCSS')

@endsection

@section('pageJS')
@endsection