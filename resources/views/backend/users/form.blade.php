@extends('backend.app')

@section('title', isset($result) ? 'Editar Usuário - ' . $result->name : 'Novo Usuário')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('/dashboard') }}"><i class="fa fa-home"></i></a>
        </li>
        <li class="breadcrumb-item" aria-current="page"><a href="{{ route('users.index') }}">Usuários Admin</a></li>
        <li class="breadcrumb-item active" aria-current="page">
            {{ isset($result) ? 'Editar Usuário' : 'Novo Usuário' }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-12">
            <div class="box">

                <div class="box-header with-border">
                    <h4 class="box-title">@yield('title')</h4>
                </div><!-- box-header -->

                <div class="box-body">
                    <!-- conteudo -->

                    <form id="form-request" method="POST" class="form-horizontal">
                        <div class="form-group">
                            <label for="name" class="col-sm-12">Nome:</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Digite seu Nome"
                                    value="{{ isset($result->name) ? $result->name : '' }}">
                            </div>
                        </div><!-- form-group -->
                        <div class="form-group">
                            <label for="email" class="col-sm-12">E-mail Válido:</label>
                            <div class="col-sm-12">
                                <input type="email" class="form-control" id="email" name="email"
                                    placeholder="Digite seu E-mail"
                                    value="{{ isset($result->email) ? $result->email : '' }}">
                            </div>
                        </div><!-- form-group -->
                        <div class="form-group">
                            <label for="password" class="col-sm-12">Senha:</label>
                            <div class="col-sm-12">
                                <input type="password" class="form-control" id="password" name="password"
                                    placeholder="Digite sua Senha de Acesso" value="">
                                <span class="help-block">* mínimo de 6 caracteres</span>
                            </div>
                        </div><!-- form-group -->
                        <div class="form-group">
                            <label for="password_confirmation" class="col-sm-12">Confirmar Senha</label>
                            <div class="col-sm-12">
                                <input type="password" class="form-control" id="password_confirmation"
                                    name="password_confirmation" placeholder="Confirmar Senha">
                            </div>
                        </div><!-- form-group -->
                    </form>
                    <!-- conteudo -->
                </div><!-- box-body -->

                <div class="box-footer flexbox">
                    <div class="text-left flex-grow">
                        <a href="javascript:;" name="salvar" id="{{ isset($result) ? 'btn-edit' : 'btn-salvar' }}"
                            data-id="{{ isset($result) ? $result->id : '' }}" class="btn btn-rounded btn-success"><i
                                class="fa fa-check"></i> Salvar</a>
                        <a href="{{ route('users.index') }}" class="btn btn-sm btn-rounded btn-info" data-toggle="tooltip"
                            data-placement="top" title="Voltar"><i class="fa fa-angle-left"></i> <span>Voltar</span></a>
                    </div>
                </div><!-- box-footer -->

            </div><!-- box -->
        </div><!-- cols -->
    </div><!-- row -->
@endsection

@section('includeCSS')
    <!-- Include SweetAlert -->
    <link rel="stylesheet" href="{{ asset('/plugins/sweetalert/sweetalert2.min.css') }}">
@endsection

@section('includeJS')
    <!-- Include SweetAlert -->
    <script src="{{ asset('/plugins/sweetalert/sweetalert2.min.js') }}"></script>
    <script>
        $(document).on('click', '#btn-salvar', function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });
            var method = 'POST';
            var url = "{{ route('users.store') }}";
            var data = $('#form-request').serialize();

            $.ajax({
                url: url,
                data: data,
                method: method,
                success: function(data) {
                    Swal.fire({
                        text: data,
                        icon: 'success',
                        showClass: {
                            popup: 'animate_animated animate_backInUp'
                        },
                        onClose: () => {
                            // Redirect Page Listagem
                            location.href = "{{ route('users.index') }}";
                        }
                    });
                },
                error: function(xhr) {
                    if (xhr.status === 422) {
                        Swal.fire({
                            text: 'Validação: ' + xhr.responseJSON,
                            icon: 'warning',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    } else {
                        Swal.fire({
                            text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
                            icon: 'error',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    }
                }
            });
        });

    </script>
    <script>
        $(document).on('click', '#btn-edit', function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });

            var data = $('#form-request').serialize();

            let id = $(this).data('id');
            var method = 'PUT';
            var url = `{{ url('dashboard/users/${id}') }}`;

            $.ajax({
                url: url,
                data: data,
                method: method,
                success: function(data) {
                    Swal.fire({
                        text: data,
                        icon: 'success',
                        showClass: {
                            popup: 'animate_animated animate_backInUp'
                        },
                        onClose: () => {
                            // Redirect Page Listagem
                            location.href = "{{ route('users.index') }}";
                        }
                    });
                },
                error: function(xhr) {
                    if (xhr.status === 422) {
                        Swal.fire({
                            text: 'Validação: ' + xhr.responseJSON,
                            icon: 'warning',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    } else {
                        Swal.fire({
                            text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
                            icon: 'error',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    }
                }
            });
        });

    </script>
@endsection
