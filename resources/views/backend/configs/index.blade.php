@extends('backend.app')

@section('title', 'Configuração Geral')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('/dashboard') }}"><i class="fa fa-home"></i></a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-12">
            <div class="box">

                <div class="box-body">
                    <!-- conteudo -->
                    <form id="form-request" method="POST" class="form-horizontal">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-6">
                                <h4 class="box-title text-info"><i class="fa fa-desktop mr-1"></i> Informações do Site</h4>
                                <hr class="my-10">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Título do Site</label>
                                            <input type="text" class="form-control" name="meta_title"
                                                placeholder="Título do Site"
                                                value="{{ isset($config->meta_title) ? $config->meta_title : '' }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Palavras Chave</label>
                                            <input type="text" class="form-control" name="meta_keywords"
                                                placeholder="Palavras Chave"
                                                value="{{ isset($config->meta_keywords) ? $config->meta_keywords : '' }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Descrição do Site</label>
                                            <textarea rows="6" class="form-control" name="meta_description"
                                                placeholder="Descrição do Site">{{ isset($config->meta_description) ? $config->meta_description : '' }}</textarea>
                                            <span class="help-block">Máximo de 255 caracteres</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row d-none">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="logo">Logo do Site</label>
                                            <div class="input-group">
                                                <input id="logo" class="form-control" type="text" name="logo"
                                                    placeholder="Selecione seu Logo">
                                                <span class="input-group-btn">
                                                    <a id="lfm" data-input="logo" data-preview="holder"
                                                        class="btn btn-primary">
                                                        <i class="fa fa-picture-o"></i> Selecionar Logo
                                                    </a>
                                                </span>
                                            </div>
                                            <div id="holder" class="border mt-3 p-3 col-8">
                                                <img src="{{ isset($config->logo) ? $config->logo : '/sem_imagem.jpg' }}"
                                                    class="img-fluid">
                                            </div><!-- holder -->
                                        </div><!-- form-group -->
                                    </div>
                                </div>
                            </div><!-- col-6 -->
                            <div class="col-sm-12 col-md-12 col-lg-6">
                                <h4 class="box-title text-info"><i class="fa fa-book mr-1"></i> Informações de Contato</h4>
                                <hr class="my-10">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nome do Site</label>
                                            <input type="text" class="form-control" name="name_site"
                                                placeholder="Nome do Site"
                                                value="{{ isset($config->name_site) ? $config->name_site : '' }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Proprietário</label>
                                            <input type="text" class="form-control" name="proprietary"
                                                placeholder="Proprietário"
                                                value="{{ isset($config->proprietary) ? $config->proprietary : '' }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>E-mail de Contato</label>
                                            <input type="text" class="form-control" name="email_admin"
                                                placeholder="E-mail de Contato"
                                                value="{{ isset($config->email_admin) ? $config->email_admin : '' }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Data do Evento</label>
                                            <input type="text" class="form-control" name="data_evento" placeholder="Telefone"
                                                value="{{ isset($config->data_evento) ? $config->data_evento : '' }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row d-none">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Telefone</label>
                                            <input type="text" class="form-control" name="telephone" placeholder="Telefone"
                                                value="{{ isset($config->telephone) ? $config->telephone : '' }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Celular</label>
                                            <input type="text" class="form-control" name="cellphone" placeholder="Celular"
                                                value="{{ isset($config->cellphone) ? $config->cellphone : '' }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row d-none">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Endereço do Site</label>
                                            <textarea rows="4" class="form-control" name="address"
                                                placeholder="Endereço do Site">{{ isset($config->address) ? $config->address : '' }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Horário de Funcionamento</label>
                                            <textarea rows="4" class="form-control" name="hour_open"
                                                placeholder="Horário de Funcionamento">{{ isset($config->hour_open) ? $config->hour_open : '' }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- col-6 -->
                        </div><!-- row -->
                    </form>
                    <!-- conteudo -->
                </div><!-- box-body -->

                <div class="box-footer flexbox">
                    <div class="text-left flex-grow">
                        <a href="javascript:;" id="btn-save" class="btn btn-rounded btn-success btn-mobile-float"
                            data-toggle="tooltip" data-placement="top" title="Salvar Alterações"><i class="fa fa-check"></i>
                            <span>Salvar Alterações</span></a>
                    </div>
                </div><!-- box-footer -->

            </div><!-- box -->
        </div><!-- cols -->
    </div><!-- row -->
@endsection

@section('includeCSS')
    <!-- Include SweetAlert -->
    <link rel="stylesheet" href="{{ asset('/plugins/sweetalert/sweetalert2.min.css') }}">
@endsection

@section('includeJS')
    <!-- FileManager Laravel -->
    <script src="{{ asset('/vendor/laravel-filemanager/js/stand-alone-button.js') }}"></script>
    <script>
        $('#lfm').filemanager('logo');
        var lfm = function(id, type, options) {
            let button = document.getElementById(id);

            button.addEventListener('click', function() {
                var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';
                var target_input = document.getElementById(button.getAttribute('data-input'));
                var target_preview = document.getElementById(button.getAttribute('data-preview'));

                window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager',
                    'width=900,height=600');
                window.SetUrl = function(items) {
                    var file_path = items.map(function(item) {
                        return item.url;
                    }).join(',');

                    // set the value of the desired input to image url
                    target_input.value = file_path;
                    target_input.dispatchEvent(new Event('change'));

                    // clear previous preview
                    target_preview.innerHtml = '';

                    // set or change the preview image src
                    items.forEach(function(item) {
                        let img = document.createElement('img')
                        img.setAttribute('style', 'height: 5rem')
                        img.setAttribute('src', item.thumb_url)
                        target_preview.appendChild(img);
                    });

                    // trigger change event
                    target_preview.dispatchEvent(new Event('change'));
                };
            });
        };

    </script>
    <!-- Include SweetAlert -->
    <script src="{{ asset('/plugins/sweetalert/sweetalert2.min.js') }}"></script>
    <script>
        $(document).on('click', '#btn-save', function(e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });

            var data = $('#form-request').serialize();

            let id = 1;
            var method = 'PUT';
            var url = `{{ url('dashboard/configs/${id}') }}`;

            $.ajax({
                url: url,
                data: data,
                method: method,
                success: function(data) {
                    Swal.fire({
                        text: data,
                        icon: 'success',
                        showClass: {
                            popup: 'animate_animated animate_backInUp'
                        },
                        onClose: () => {
                            // Redirect Page Listagem
                            location.href = "{{ route('configs') }}";
                        }
                    });
                },
                error: function(xhr) {
                    if (xhr.status === 422) {
                        Swal.fire({
                            text: 'Validação: ' + xhr.responseJSON,
                            icon: 'warning',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    } else {
                        Swal.fire({
                            text: 'Erro interno, informe ao suporte: ' + xhr.responseJSON,
                            icon: 'error',
                            showClass: {
                                popup: 'animate_animated animate_wobble'
                            }
                        });
                    }
                }
            });
        });

    </script>
@endsection
