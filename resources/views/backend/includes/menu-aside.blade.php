<aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar">

        <div class="user-profile">
            <div class="logo-painel mt-3">
                <a href="{{ url('dashboard') }}">
                    <!-- logo for regular state and mobile devices -->
                    <div class="d-none d-md-flex align-items-center justify-content-center">
                        <img src="{{ asset('galerias/logo_innsystem.png') }}"
                        alt="{{$config_site->meta_title}}" class="img-fluid">
                    </div>
                </a>
            </div>
        </div>

        <!-- sidebar menu-->
        <ul class="sidebar-menu" data-widget="tree">

            <li class="">
                <a href="{{ url('/') }}" target="_Blank">
                    <i class="fa fa-desktop"></i>
                    <span>Meu Site</span>
                </a>
            </li>


            <li class="@if(collect(request()->segments())->last() == 'dashboard') active @endif">
                <a href="{{ url('/dashboard') }}">
                    <i class="fa fa-pie-chart"></i>
                    <span>Página Inicial</span>
                </a>
            </li>



            <li class="header nav-small-cap">Área Administrativa</li>
            
            <li class="@if(collect(request()->segments())->last() == 'users') active @endif d-none">
                <a href="{{ route('users.index') }}">
                    <i class="fa fa-user-shield"></i>
                    <span>Usuários Admin</span>
                </a>
            </li>
            <li class="@if(collect(request()->segments())->last() == 'configs') active @endif">
                <a href="{{ route('configs') }}">
                    <i class="fa fa-cogs"></i>
                    <span>Configuração Geral</span>
                </a>
            </li>
            
            <li class="treeview d-none">
                <a href="#">
                    <i class="fa fa-user-shield"></i>
                    <span>Usuários Admin</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="chat.html"><i class="fa fa-more"></i>Chat</a></li>
                    <li><a href="calendar.html"><i class="fa fa-more"></i>Calendar</a></li>
                </ul>
            </li>

            <li class="header nav-small-cap">Conteúdo do Site</li>

            <li class="@if(collect(request()->segments())->last() == 'pages') active @endif d-none">
                <a href="{{ route('pages.index') }}">
                    <i class="fa fa-book"></i>
                    <span>Páginas de Informações</span>
                </a>
            </li>

        </ul>
    </section>

    <div class="sidebar-footer">
        <!-- item-->
        <a href="{{ url('/dashboard') }}" class="link" data-toggle="tooltip" title="" data-original-title="Início"><i
                class="fa fa-home"></i></a>

        <!-- item-->
        <a href="javascript:void(0)" class="link" data-toggle="tooltip" title="" data-original-title="Sair da Conta"><i
                class="fa fa-sign-out"></i></a>
    </div>
</aside>
