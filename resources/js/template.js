//-- go to top button -- //
$(document).ready(function() {
    $('body').append('<div id="toTop" class="btn"><span class="fa fa-chevron-up"></span></div>');
    $(window).scroll(function() {
        if ($(this).scrollTop() != 0) {
            $('#toTop').fadeIn();
        } else {
            $('#toTop').fadeOut();
        }
    });
    $('#toTop').click(function() {
        $("html, body").animate({
            scrollTop: 0
        }, 300);
        return false;
    });
});