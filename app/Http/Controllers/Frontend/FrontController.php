<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;

class FrontController extends Controller
{
    /* Variables globais */
    public $folder = 'frontend.';

    public function index()
    {
        return view($this->folder . 'home');
    }

    public function single()
    {
        return view($this->folder . 'single');
    }

    public function obrigado()
    {
        return view($this->folder . 'obrigado');
    }

    public function newcontact(Request $request)
    {
        $result = $request->all();

        $rules = [
            'name'          => "required",
            'email'          => "required",
        ];

        $messages = [
            'name.required'       => 'Nome é obrigatório',
            'email.required'       => 'E-mail é obrigatório',
        ];

        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }

        try {
            // ActiveCampaign
            $active_url = env('ACTIVECAMPAIGN_API_URL');
            $active_key = env('ACTIVECAMPAIGN_API_KEY');
            $active_api = env('ACTIVECAMPAIGN_API_VERSION');

            Http::withHeaders([
                'Accept' => 'application/json',
                'Api-Token' => $active_key
            ])->post($active_url . $active_api . '/contact/sync', [
                'contact' => [
                    'email' => $result['email'],
                    'firstName' => $result['name']
                ]
            ]);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('Inscrição salva com sucesso', 200);
    }


    

    public function newcontact_sendinblue()
    {
        // SendinBlue
        $config = \SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', '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');

        $apiInstance = new \SendinBlue\Client\Api\ContactsApi(
            new \GuzzleHttp\Client(),
            $config
        );

        $createContact = new \SendinBlue\Client\Model\CreateContact(); // Values to create a contact
        $createContact['email'] = 'contato@innsystem.com.br';
        $createContact['attributes'] =
            [
                "NOME" => "Leonardo",
                "SOBRENOME"  => "Augusto"
            ];

        $createContact['listIds'] = [1];

        try {
            $result = $apiInstance->createContact($createContact);
            print_r($result);
        } catch (\Exception $e) { // com ou sem a \ no Exception
            print_r($e);
            echo 'Exception when calling ContactsApi->createContact: ', $e->getMessage(), PHP_EOL;
        }
    }
}
