<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\Page;

class PagesController extends Controller
{
    /* Variables globais */
    public $folder = 'backend.pages.';

    /**
     * Display a listing of the resource.
     * // INDEX
     */
    public function index(Request $request)
    {
        $itensPage = $request->input('itensPage') ? $request->input('itensPage') : '5';

        $pages = Page::paginate($itensPage);

        return view($this->folder . 'index', [
            'pages' => $pages,
            'itensPage' => $itensPage,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * // CREATE
     */
    public function create()
    {
        return view($this->folder . 'form');
    }

    /**
     * Store a newly created resource in storage.
     * // STORE
     */
    public function store(Request $request)
    {
        $model = new Page;
        $result = $request->all();

        $rules = [
            'meta_title'          => "required",
            'meta_description'    => "nullable|max:255",
            'meta_keywords'       => "nullable|max:255",
            'content'             => "required",
            'slug'                => "required|unique:pages",
            'status'              => "required",
        ];

        $messages = [
            'meta_title.required'       => 'título é obrigatório',
            'meta_description.max'      => 'descrição precisa ter no máximo 255 caracteres',
            'meta_keywords.max'         => 'palavras-chave precisa ter no máximo 255 caracteres',
            'content.required'          => 'conteúdo é obrigatório',
            'slug.required'             => 'url amigável é obrigatório',
            'slug.unique'               => 'url amigável já existe',
            'status.required'           => 'status é obrigatório',
        ];

        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }

        $model->meta_title = $result['meta_title'];
        $model->meta_description = $result['meta_description'];
        $model->meta_keywords = $result['meta_keywords'];
        $model->content = $result['content'];
        $model->slug = $result['slug'];
        $model->status = $result['status'];

        try {
            $model->save();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('Pagina salva com sucesso', 200);
    }

    /**
     * Display the specified resource.
     * // SHOW
     */
    public function show($slugPage)
    {
        $page = new Page;
        $result = $page->where('slug', $slugPage)->first();

        return view($this->folder . 'show', compact('result'));
    }

    /**
     * Show the form for editing the specified resource.
     * // EDIT
     */
    public function edit($id)
    {
        $page = new Page;
        $result = $page->where('id', $id)->first();

        return view($this->folder . 'form', compact('result'));
    }

    /**
     * Update the specified resource in storage.
     * // UPDATE
     */
    public function update(Request $request, $id)
    {
        $model = new Page;
        $model = $model::find($id);

        $result = $request->all();

        $rules = [
            'meta_title'          => "required",
            'meta_description'    => "nullable|max:255",
            'meta_keywords'       => "nullable|max:255",
            'content'             => "required",
            'slug'                => "required|unique:pages,slug,$id,id",
            'status'              => "required",
        ];

        $messages = [
            'meta_title.required'       => 'título é obrigatório',
            'meta_description.max'      => 'descrição precisa ter no máximo 255 caracteres',
            'meta_keywords.max'         => 'palavras-chave precisa ter no máximo 255 caracteres',
            'content.required'          => 'conteúdo é obrigatório',
            'slug.required'             => 'url amigável é obrigatório',
            'slug.unique'               => 'url amigável já existe',
            'status.required'           => 'status é obrigatório',
        ];

        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }

        $model->meta_title = $result['meta_title'];
        $model->meta_description = $result['meta_description'];
        $model->meta_keywords = $result['meta_keywords'];
        $model->content = $result['content'];
        $model->slug = $result['slug'];
        $model->status = $result['status'];

        try {
            $model->save();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('Pagina alterada com sucesso', 200);
    }

    /**
     * Remove the specified resource from storage.
     * // DESTROY
     */
    public function destroy($id)
    {
        $model = new Page;

        try {
            $find = $model->find($id);
            $find->delete();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json(true, 200);
    }
}
