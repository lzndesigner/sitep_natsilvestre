<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Config;

class ConfigsController extends Controller
{
    /* Variables globais */
    public $folder = 'backend.configs.'; // required ponto final

    public function index()
    {
        $config = Config::first();
        return view($this->folder . 'index', [
            'config' => $config
        ]);
    }

    public function update(Request $request, $id)
    {
        $model = new Config;
        $model = $model::find($id);

        $result = $request->all();

        $rules = [
            'meta_title'        => 'required',
            'meta_description'  => 'max:500',
            'name_site'         => 'required',
            'proprietary'       => 'required',
            'email_admin'       => 'required|email',
            'logo'              => 'nullable',
            'data_evento'              => 'nullable',
        ];

        $messages = [
            'meta_title.required' => 'título é obrigatório',
            'meta_description.max' => 'máximo de 500 caracteres',
            'name_site.required' => 'nome do site é obrigatório',
            'proprietary.required' => 'proprietário é obrigatório',
            'email_admin.required' => 'e-mail é obrigatório',
            'logo.required' => 'logo é obrigatório',
            'data_evento.required' => 'logo é obrigatório',
        ];

        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }

        $model->meta_title = $result['meta_title'];
        $model->meta_description = $result['meta_description'];
        $model->meta_keywords = $result['meta_keywords'];
        $model->name_site = $result['name_site'];
        $model->proprietary = $result['proprietary'];
        $model->address = $result['address'];
        $model->email_admin = $result['email_admin'];
        $model->telephone = $result['telephone'];
        $model->cellphone = $result['cellphone'];
        $model->hour_open = $result['hour_open'];
        $model->data_evento = $result['data_evento'];
        if($result['logo']){
            $format_url_logo = str_replace(env('APP_URL'), '', $result['logo']);
            $model->logo = $format_url_logo;
        }

        try {
            $model->save();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('Configurações alteradas com sucesso', 200);
    }
}
