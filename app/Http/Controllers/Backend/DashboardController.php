<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /* Variables globais */
    public $folder = 'backend.';

    public function index(){
        return view($this->folder.'dashboard');
    }
}
