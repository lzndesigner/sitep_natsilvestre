<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserController extends Controller
{
    /* Variables globais */
    public $folder = 'backend.users.';

    /**
     * Display a listing of the resource.
     * // INDEX
     */
    public function index(Request $request)
    {
        $itensPage = $request->input('itensPage') ? $request->input('itensPage') : '5';

        $users = User::paginate($itensPage);

        return view($this->folder . 'index', [
            'users' => $users,
            'itensPage' => $itensPage,
        ]);
    }

    /**
     * Result Search.
     * // SEARCH
     */
    public function search(Request $request)
    {
        $output = null;
        $search = $request->input('search');

        if ($search) {
            $users = User::where('name', 'like', '%' . $search . '%')->orWhere('email', 'like',  '%' . $search . '%')->get();
        }else{
            return response()->json('Digite um nome ou e-mail', 422);
        }

        if(!$users->isEmpty()){
            foreach ($users as $key => $user) {
                $output .= '<tr>' .
                    '<td>' . $user->id . '</td>' .
                    '<td>' . $user->name . '</td>' .
                    '<td>' . $user->email . '</td>' .
                    '<td>' . \Carbon\Carbon::parse($user->created_at)->format('d/m/y H:i') . '</td>' .
                    '<td><a href="/dashboard/users/'.$user->id.'/edit" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
                    <a href="javascript:;" data-id="' . $user->id . '" class="btn btn-sm btn-danger btn-delete"><i class="fa fa-trash"></i></a></td>' .
                    '</tr>';
            }
            return response()->json($output, 200);
        } else {
            return response()->json('Nenhum resultado foi encontrado.', 422);
        }
    }


    /**
     * Show the form for creating a new resource.
     * // CREATE
     */
    public function create()
    {
        return view($this->folder . 'form');
    }

    /**
     * Store a newly created resource in storage.
     * // STORE
     */
    public function store(Request $request)
    {
        $model = new User;
        $result = $request->all();

        $rules = [
            'name'          => "required",
            'email'         => "required|unique:users",
            'password'      => 'required|min:6|confirmed',
        ];

        $messages = [
            'name.required' => 'nome é obrigatório',
            'email.required' => 'e-mail é obrigatório',
            'password.required' => 'senha é obrigatório',
            'password.min' => 'senha precisa ter 6 caracteres',
        ];

        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }

        $model->name = $result['name'];
        $model->email = $result['email'];
        $model->password = Hash::make($result['password']);

        try {
            $model->save();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('Cliente salvo com sucesso', 200);
    }

    /**
     * Display the specified resource.
     * // SHOW
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * // EDIT
     */
    public function edit($id)
    {
        $user = new User;
        $result = $user->where('id', $id)->first();

        return view($this->folder . 'form', compact('result'));
    }

    /**
     * Update the specified resource in storage.
     * // UPDATE
     */
    public function update(Request $request, $id)
    {
        $model = new User;
        $model = $model::find($id);

        $result = $request->all();

        $rules = [
            'name'          => "required",
            'email'         => "unique:users,email,$id,id",
            'password'      => 'nullable|min:6|confirmed',
        ];

        $messages = [
            'name.required' => 'nome é obrigatório',
            'email.required' => 'e-mail é obrigatório',
            'email.unique' => 'e-mail já está em uso',
            'password.confirmed' => 'as senhas não são igual',
        ];

        $validator = Validator::make($result, $rules, $messages);

        if ($validator->fails()) {
            return response()->json($validator->errors()->first(), 422);
        }

        $model->name = $result['name'];
        $model->email = $result['email'];
        if ($result['password']) {
            $password = Hash::make($result['password']);
            $model->password = $password;
        }

        try {
            $model->save();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json('Cliente alterado com sucesso', 200);
    }

    /**
     * Remove the specified resource from storage.
     * // DESTROY
     */
    public function destroy($id)
    {
        $model = new User;

        try {
            $find = $model->find($id);
            $find->delete();
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }

        return response()->json(true, 200);
    }
}
