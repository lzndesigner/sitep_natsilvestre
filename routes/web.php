<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Frontend\{
    FrontController
};
use App\Http\Controllers\Backend\{
    DashboardController,
    ManagerController,
    UserController,
    ConfigsController,
    PagesController,
};

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [FrontController::class, 'single'])->name('homepage');
// Route::get('/vendas', [FrontController::class, 'index'])->name('home');


// Route::get('/obrigado', [FrontController::class, 'obrigado'])->name('obrigado');;
// Route::post('/newcontact', [FrontController::class, 'newcontact'])->name('newcontact');;



Auth::routes();

Route::prefix('dashboard')->middleware('auth')->group(function () {
    Route::resource('/', DashboardController::class);
    Route::group(['prefix' => 'laravel-filemanager'], function () {
        \UniSharp\LaravelFilemanager\Lfm::routes();
    });

    Route::resource('users', UserController::class);
    Route::get('users/showsearch', [UserController::class, 'showsearch'])->name('users.showsearch');
    Route::post('users/search', [UserController::class, 'search'])->name('users.search');
    
    Route::get('configs', [ConfigsController::class, 'index'])->name('configs');
    Route::put('configs/{id}', [ConfigsController::class, 'update'])->name('configs.update');
    
    Route::resource('pages', PagesController::class);
    Route::get('pages/{slugPage}', [PagesController::class, 'show']);

});
