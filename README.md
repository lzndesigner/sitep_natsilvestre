<p align="center">
<a href="https://innsystem.com.br" target="_blank">
<img src="https://innsystem.com.br/galerias/logo_preto_azul.png" width="400" alt="InnSystem">
</a>
</p>


## About - Painel Base Laravel

Usando o Laravel 8 desenvolvi um painel que servirá como base para iniciar projetos de clientes.

## Como Instalar

clone projeto

na pasta rode os comandos:
composer install

criar banco de dados
criar arquivo .env

na pasta rode os comandos:
php artisan key:generate
php artisan migrate --seed
php artisan storage:link
